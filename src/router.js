import Vue from 'vue';
import Router from 'vue-router';

//Home Views
import Home from './views/Home.vue';

//Gym Views
import Abilities from './views/Gym/Abilities.vue';
import Moves from './views/Gym/Moves.vue';
import Stats from './views/Gym/Stats.vue';
import AdvancedTechniques from './views/Gym/AdvancedTechniques.vue';
import SpecialAbilities from './views/Gym/SpecialAbilities.vue';
import Trademarks from './views/Gym/Trademarks.vue';
import Taunts from './views/Gym/Taunts.vue';
import Finishers from './views/Gym/Finishers.vue';
import PrivateGym from './views/Gym/PrivateGym.vue';
import SparringPartner from './views/Gym/SparringPartner.vue';
import SparringResult from './views/Gym/SparringResult.vue';

//Arena Views
import Search from './views/Arena/Search.vue';
import Fight from './views/Arena/Fight.vue';
import Wgc from './views/Arena/WGC.vue';
import Tournaments from './views/Arena/Tournaments.vue';
import Events from './views/Arena/Events.vue';
import PrivateLeagues from './views/Arena/PrivateLeagues.vue';

//Shop views
import Supporter from './views/Shop/Supporter.vue';


Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },

    //GYM routes
    {
      path: '/gym/private_gym',
      name: 'private_gym',
      component: PrivateGym
    },
    {
      path: '/gym/sparring_partner/',
      name: 'sparring_partner',
      component: SparringPartner
    },
    {
      path: '/gym/sparring_result/',
      name: 'sparring_result',
      component: SparringResult
    },
    {
      path: '/gym/trainer/abilities',
      name: 'abilities',
      component: Abilities
    },
    {
      path: '/gym/trainer/moves',
      name: 'moves',
      component: Moves
    },
    {
      path: '/gym/trainer/stats',
      name: 'stats',
      component: Stats
    },
    {
      path: '/gym/trainer/trademarks',
      name: 'trademarks',
      component: Trademarks
    },
    {
      path: '/gym/trainer/taunts',
      name: 'taunts',
      component: Taunts
    },
    {
      path: '/gym/trainer/advanced_techniques',
      name: 'advanced_techniques',
      component: AdvancedTechniques
    },
    {
      path: '/gym/trainer/special_abilities',
      name: 'special_abilities',
      component: SpecialAbilities
    },
    {
      path: '/gym/trainer/finishers',
      name: 'finishers',
      component: Finishers
    },
    //ARENA Routes
  
    {
      path: '/arena/search',
      name: 'search',
      component: Search
    },
    {
      path: '/arena/match/:id',
      name: 'match',
      component: Fight
    },
    {
      path: '/arena/wgc',
      name: 'wgc',
      component: Wgc
    },
    {
      path: '/arena/tournaments',
      name: 'tournaments',
      component: Tournaments
    },
    {
      path: '/arena/events',
      name: 'events',
      component: Events
    },
    {
      path: '/arena/private_leagues',
      name: 'private_leagues',
      component: PrivateLeagues
    },

    //Shop Routes
    {
      path: '/shop/supporter',
      name: 'shop_supporter',
      component: Supporter
    },

  ]

});
