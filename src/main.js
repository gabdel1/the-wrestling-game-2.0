import './assets/sass/style.scss';

import Vue from 'vue';
import VTooltip from 'v-tooltip';
import App from './App.vue';
import router from './router';
import store from './store';

import enLang from '@/assets/lang/en.json';

Vue.use(VTooltip);
VTooltip.options.defaultPlacement = 'auto';

Vue.mixin({
  data(){
    return {
      tickerInterval: {},
      ticker: 1,
      info: {}
    }
  },
  created(){
    
    this.fetchInfo();
    
    this.tickerInterval = setInterval(() => {
      this.ticker++;
    }, 1000);

  },
  computed:{
    lang(){
      return enLang;
    },
    fullPath(){
      // return "";
      return "https://test2.thewrestlinggame.com";
    }
  },
  methods: {
    fetchInfo() {
      fetch(this.fullPath + "/wg/api/?get_data=header")
        .then(response => response.json())
        .then(json => {
          this.info = json
        });
    }, 
    hasData(obj) {
      return Object.keys(obj).length;
    },
    getPercentage(divider, dividee){
      return Math.round((dividee / divider) * 100);
    }
  },
  beforeDestroy(){
    clearInterval(this.tickerInterval);
  },

});

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
