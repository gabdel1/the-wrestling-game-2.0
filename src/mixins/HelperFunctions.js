export default {
    methods:{
        floatToPercentage(float){
            return +(float * 100).toFixed(1) + "%";
        },
        nullToNone(string){
            return string == null || string == "" ? "None" : string;
        },
        timestampToTimer(ts){
            var timer = {};
            timer.seconds = ts;
            timer.minutes = Math.floor(timer.seconds/60);
            timer.seconds = timer.seconds - (timer.minutes * 60);
            timer.hours = Math.floor(timer.minutes/60);
            timer.minutes = timer.minutes - (timer.hours * 60);
            timer.days = Math.floor(timer.hours/24);
            timer.hours = timer.hours - (timer.days * 24);

            if(timer.days)
                return timer.days + " days " + timer.hours + " hours"; 
            
            if(timer.hours)
                return timer.hours + " hours " + timer.minutes + " minutes";

            return (timer.minutes ? timer.minutes : "0") + " minutes " + timer.minutes + " seconds";
        },
        getClassIcon(cls){
            switch(cls){
                case 'strength':
                return 'str';

                case 'speed':
                return 'spd';

                case 'technique':
                return 'tech';

                case 'resistance':
                return 'endr';

                case 'balanced':
                return 'blc';
            }
        }
    }
}